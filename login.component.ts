import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponent } from 'src/app/app.component';
import { MessagesService } from 'src/app/common/services/messages-data-service.service';
import { MessagingService } from 'src/app/services/messages.service';
import { SessionComponent } from './../../common/components/session-component.component';



@Component({
	selector: 'login',
	templateUrl: './login.component.html',
	styleUrls: ['login.component.less']
})
export class LoginComponent extends SessionComponent implements OnInit {
	isError: boolean = false;
	ngOnInit(): void {
		if (this.authService.isLoggedIn) {
			this.router.navigate(['home']);
		}
	}

	constructor(private router: Router,
		private app: AppComponent,
		private messagingService: MessagingService,
		messagesService: MessagesService
	) {
		super(messagesService);
		app.activeMenu = '';
	}

	facebookLogin(): void {
		this.addLoadingCount();
		this.authService.loginFacebook().then((data) => {
			console.log(data);
			if (data.username) {
				this.router.navigate([""]);
				this.susLoadingCount();
			} else {
				console.log(data);
				this.isError = true;
				this.susLoadingCount();
			}
		}).catch(err => {
			this.isError = true;
			this.susLoadingCount();
		})
	}

	googleLogin(): void {
		this.addLoadingCount();
		this.authService.loginGoogle().then((data) => {
			console.log(data);
			if (data.username) {
				this.router.navigate([""]);
				this.susLoadingCount();
			} else {
				console.log(data);
				this.isError = true;
				this.susLoadingCount();
			}
		}).catch(err => {
			this.isError = true;
			this.susLoadingCount();
		})
	}




}
