import { AfterViewInit, Component, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { SessionComponent } from 'src/app/common/components/session-component.component';
import { AppComponent } from 'src/app/app.component';
import { CantidadPax } from 'src/app/model/CantidadPax';
import { Reserva } from 'src/app/model/Reserva';
import { ItemReserva } from 'src/app/model/ItemReserva';
import { DatosPasajero } from 'src/app/model/DatosPasajero';
import { ReservaService } from 'src/app/services/reserva.service';
import { debounceTime, map, startWith, take } from 'rxjs/operators';
import { MatStepper } from '@angular/material/stepper';
import { azulData } from 'src/app/model/azulData';
import { Cliente } from 'src/app/model/Cliente';
import { Descriptivo } from 'src/app/common/model/Descriptivo';
import { Nacionalidad } from 'src/app/model/Nacionalidad';
import { NacionalidadService } from 'src/app/services/nacionalidad.service';
import { Observable } from 'rxjs';
import { Filtro } from 'src/app/common/model/Filtro';
import { MatRadioChange } from '@angular/material/radio';

declare let paypal;
@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.less']
})
export class CheckoutComponent extends SessionComponent implements OnInit, AfterViewInit {
  @ViewChildren('paypal') ref: QueryList<any>;
  @ViewChild('stepper') stepper: MatStepper;
  editable: boolean = true;
  resumenEditable: boolean = true;
  reservaForm: FormGroup;
  metodoPago: string = '';
  productoDescripcion: string = '';
  tokenReserva: string = '';
  detalleItem: any;
  tipoProducto: any;
  reserva: Reserva = new Reserva;
  itemReserva: ItemReserva = new ItemReserva;
  cantidadPax: CantidadPax[] = [];
  tipoPago: string = '';
  azulData: azulData = { token: '', precio: 0};
  isCompleted: boolean = false;
  dataReserva: Reserva;

  filtroNac: Filtro = new Filtro('dummy', {});
  

  nacionalidades: any[] = [];
  filteredNacionalidades: Array<Observable<string[]>> = [];
  nacionalidad: any;

  constructor(
    private app: AppComponent,
    private fb: FormBuilder,
    private reservaService: ReservaService,
    private nacionalidadService: NacionalidadService
  ) {
    super();
    app.activeMenu = '';
  }

  ngAfterViewInit() {
    this.ref.changes.pipe(take(1)).subscribe( result => {
      paypal
      .Buttons({
        createOrder: (data, actions) => {
          return actions.order.create({
            purchase_units: [
              {
                description: this.producto,
                amount: {
                  currency_code: 'USD',
                  value: this.precioTotal
                }
              }
            ]
          })
        },
        onApprove: (data, actions) => {
          return actions.order.capture().then((details) => {
            // Transacción OK
            console.log(details);
            this.reserva.codigoPago = details['purchase_units'][0].payments.captures[0].id;
            this.reserva.medioPago = "paypal";
            this.isCompleted = true;
            this.makeReserva();
          });
        },
        onError: (err) => {
          // Manejar error
          console.log(err);
        }
      })
      .render(result.first.nativeElement)
    })
  }
  
  ngOnInit() {
    window.scroll({
      top: 0,
      behavior: 'smooth'
    });
    this.detalleItem = this.app.detalleProducto;
    console.log(this.detalleItem);
    this.reservaForm = this.fb.group({
      vueloIda: '',
      empresaIda: '',
      horarioIda: '',
      vueloVuelta: '',
      empresaVuelta: '',
      horarioVuelta: '',
      comentarios: '',
      telefono: '',
      email: '',
      pasajeros: this.fb.array([
        
      ]),
    })
    this.filtroNac.size = 250;
    this.nacionalidadService.getAll(this.filtroNac).then(n => {
      this.nacionalidades = n;
      for (var i = 0; i < this.paxForms.length; i++) {
        this.filteredNacionalidades[i] = this.paxForms.at(i).get("nacionalidad").valueChanges
          .pipe(
          startWith(''),
          map(value => this._filter(value))
        );
      }
    })
    this.buildProductoData();
    this.calcularPax();
    this.tokenReserva = Math.random().toString(36).substr(2, 7);
    this.azulData.token = this.tokenReserva;
    localStorage.setItem("tokenReserva", this.tokenReserva);
  }


  get producto () {
    return this.detalleItem?.producto.descripcion;
  }

  get filtro() {
    return this.app.filtroAplicado;
  }

  get esIdaYVuelta() {
    if (this.productoDescripcion === 'Traslado') {
      return this.app.filtroAplicado.esIdaYVuelta;
    }
  }

  get paxForms() {
    return this.reservaForm.get('pasajeros') as FormArray;
  }

  get paxUno() {
    return this.reservaForm.get('pasajeros.0').value;
  }

  get desde() {
    if (this.productoDescripcion === 'Traslado') {
      return JSON.parse(localStorage.getItem("desde"));
    }
    if (this.productoDescripcion === 'Excursión') {
      return this.app.filtroAplicado.zonaDesde;
    }
  }

  get hasta() {
    if (this.productoDescripcion === 'Traslado') {
      return JSON.parse(localStorage.getItem("hasta"));
    }
  }


  get precioTotal() {
		let precioFinal = this.precioSubTotal(this.detalleItem?.precio) + this.precioSubTotal(this.detalleItem?.precio?.precioVuelta);
		return precioFinal;
	}

	precioSubTotal(precio: any) {
		let precioFinal = precio?.precioConDescuentoTotal || precio?.precioListaTotal;
		
		return precioFinal || 0;
	}


  buildProductoData(): void {
    console.log(this.detalleItem.producto);
    switch (this.detalleItem.producto.nombreEntidad) {
      case 'TrasladoVO':
        this.productoDescripcion = 'Traslado'
        break;
      case 'ExcursionVO':
        this.productoDescripcion = 'Excursión'
        break;
      default:
        this.productoDescripcion = 'Traslado';
        break;
    }
    this.tipoProducto = JSON.parse(localStorage.getItem("tipoProducto")).find(producto => producto.descripcion === this.productoDescripcion);
  }

  buildForm() {
    this.cantidadPax.forEach(p => {
      this.paxForms.push(new FormGroup({
        nombre: new FormControl(''),
        apellido: new FormControl(''),
        nacionalidad: new FormControl(''),
        identificacion: new FormControl(''),
        edad: new FormControl(''),
      }));
    })

  }

  calcularPax():void {
    this.filtro.cantidades.forEach(pax => {
      for (let i = 0; i < pax.cantidad; i++) {
        let p = new CantidadPax(pax.edad, 1);
        this.cantidadPax.push(p);
      }
    })
    this.buildForm();
  }

  generarReserva(): void {
    if (this.reservaForm.valid) {
      if (this.app.usuario) {
        this.reserva.cliente = new Descriptivo(this.app.usuario.codigo, this.app.usuario.email, this.app.usuario.id);
      } else {
        this.reserva.cliente = null;
      }
      this.reserva.valorTotal = this.precioTotal;
      this.reserva.descripcion = `${this.paxUno.nombre} ${this.paxUno.apellido}`;
      if (this.productoDescripcion === 'Excursión') {
        this.reserva.valorDeReserva = this.reserva.valorTotal;
        this.buildItemReservaExcursion();
      } else if (this.productoDescripcion === 'Traslado') {
        if (this.tipoPago === 'destino') {
          this.reserva.valorDeReserva = null;
        } else {
          this.reserva.valorDeReserva = this.reserva.valorTotal;
        }
        this.buildItemReservaTraslado();
      }
      localStorage.setItem("reserva", JSON.stringify(this.reserva));
      this.azulData.precio = this.reserva.valorTotal;
    }
  }

  makeReserva(): void {
    if (this.tipoPago === 'destino') {
      this.isCompleted = true;
    }
    this.reservaService.createReserva(this.reserva).then(response => {
      if (response) {
        localStorage.removeItem("tokenReserva");
        localStorage.removeItem("reserva");
        this.dataReserva = response;
        this.resumenEditable = false;
        this.stepper.next();
      }
    })
  }

  buildItemReservaTraslado(): void {
    let item = new ItemReserva;
    item.tipoProducto = this.tipoProducto;
    item.producto = this.app.filtroAplicado.json.producto;
    item.opcion = this.app.filtroAplicado.json.producto;
    item.datosPasajeros = [];
    item.cantidadPax = this.cantidadPax.length;
    item.valorTotal = this.precioTotal;
    item.fechaDesde = this.app.filtroAplicado.json.fecha;
    item.esIdaYVuelta = this.app.filtroAplicado.esIdaYVuelta;
    item.fechaHasta = this.app.filtroAplicado.json.fechaHasta !== 'undefined' ? this.app.filtroAplicado.json.fechaHasta : undefined;
    item.puntoConexionDesde =  this.app.filtroAplicado.json.zonaDesde;
    item.puntoConexionHasta = this.app.filtroAplicado.json.zonaHasta;
    item.vueloIda = this.reservaForm.get('vueloIda').value;
    item.empresaIda = this.reservaForm.get('empresaIda').value;
    item.horarioIda = this.reservaForm.get('horarioIda').value;
    item.vueloVuelta = this.reservaForm.get('vueloVuelta').value;
    item.empresaVuelta = this.reservaForm.get('empresaVuelta').value;
    item.horarioVuelta = this.reservaForm.get('horarioVuelta').value;
    item.comentarios = this.reservaForm.get('comentarios').value;
    item.telefono = this.reservaForm.get('telefono').value;
    item.email = this.reservaForm.get('email').value;
    this.paxForms.value.forEach(pax => {
      item.datosPasajeros.push( new DatosPasajero(pax.nombre, pax.apellido, pax.nacionalidad, pax.identificacion, pax.edad))
    })
    this.reserva.itemsReserva.push(item);
  }

  buildItemReservaExcursion(): void {
    let item = new ItemReserva;
    item.tipoProducto = this.tipoProducto;
    item.producto = this.app.filtroAplicado.json.producto;
    item.opcion = this.app.filtroAplicado.json.opcion;
    item.datosPasajeros = [];
    item.cantidadPax = this.cantidadPax.length;
    item.valorTotal = this.precioTotal;
    item.fechaDesde = this.app.filtroAplicado.json.fecha;
    item.esIdaYVuelta = this.app.filtroAplicado.esIdaYVuelta;
    item.fechaHasta = this.app.filtroAplicado.json.fechaHasta !== 'undefined' ? this.app.filtroAplicado.json.fechaHasta : undefined;
    item.puntoConexionDesde =  this.app.filtroAplicado.json.zonaDesde;
    item.puntoConexionHasta = this.app.filtroAplicado.json.zonaHasta;
    item.vueloIda = this.reservaForm.get('vueloIda').value;
    item.empresaIda = this.reservaForm.get('empresaIda').value;
    item.horarioIda = this.reservaForm.get('horarioIda').value;
    item.vueloVuelta = this.reservaForm.get('vueloVuelta').value;
    item.empresaVuelta = this.reservaForm.get('empresaVuelta').value;
    item.horarioVuelta = this.reservaForm.get('horarioVuelta').value;
    item.comentarios = this.reservaForm.get('comentarios').value;
    item.telefono = this.reservaForm.get('telefono').value;
    item.email = this.reservaForm.get('email').value;
    this.paxForms.value.forEach(pax => {
      item.datosPasajeros.push( new DatosPasajero(pax.nombre, pax.apellido, pax.nacionalidad, pax.identificacion, pax.edad))
    })
    this.reserva.itemsReserva.push(item);
  }

  displayFn(nacionalidad: any): string {
		return nacionalidad && nacionalidad.descripcion ? nacionalidad.descripcion : '';
    }
    
    private _filter(val?: string): string[] {
      return this.nacionalidades.filter(nacionalidad =>
        nacionalidad.descripcion.toLowerCase().startsWith(String(val).toLowerCase()))
      }

      radioChange($event: MatRadioChange): void {
        console.log($event.value);
        this.generarReserva();
      }

}
