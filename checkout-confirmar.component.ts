import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { filter } from 'rxjs/operators';
import { Reserva } from 'src/app/model/Reserva';
import { ReservaService } from 'src/app/services/reserva.service';

@Component({
  selector: 'app-confirmar',
  templateUrl: './checkout-confirmar.component.html',
  styleUrls: ['./checkout-confirmar.component.less']
})
export class CheckoutConfirmarComponent implements OnInit {

  reserva: Reserva;
  paramToken: string;
  working: boolean;
  problem: boolean;
  codigoAzul: string = '';

  constructor(
    private activatedRoute: ActivatedRoute,
    private reservaService: ReservaService
  ) {
    this.activatedRoute.queryParams.subscribe(queryParams => {
      this.codigoAzul = queryParams['AzulOrderId'];
    })
    this.activatedRoute.params.subscribe(param => {
      this.paramToken = param['token'];
    })
   }

  ngOnInit() {
    this.working = true;
    if (this.paramToken === this.tokenStorage) {
      this.reserva = this.reservaStorage;
      this.reserva.medioPago = "Azul";
      this.reserva.codigoPago = this.codigoAzul;
      this.makeReserva();
    } else {
      this.working = false;
      this.problem = true;
    }
  }

  get tokenStorage() {
    return localStorage.getItem("tokenReserva");
  }

  get reservaStorage() {
    return JSON.parse(localStorage.getItem("reserva"));
  }

  makeReserva(): void {
    this.reservaService.createReserva(this.reserva).then(response => {
      if (response) {
        console.log(response);
        localStorage.removeItem("tokenReserva");
        localStorage.removeItem("reserva");
        this.working = false;
      }
    })
  }

}
