import { Component, OnInit } from '@angular/core';
import { SessionComponent } from 'src/app/common/components/session-component.component';
import { AppComponent } from 'src/app/app.component';
import { FiltroTraslado } from 'src/app/model/FiltroTraslado';
import { ListaPreciosService } from 'src/app/services/lista-precios.service';
import { PresentadorPreciosService } from 'src/app/services/presentador-precios.service';
import Constantes from 'src/app/model/const/Constantes';
import { TarjetaTraslado } from 'src/app/model/TarjetaTraslado';
import { PuntoConexionService } from 'src/app/services/punto-conexion.service';
import { RegionService } from 'src/app/common/services/region.service';
import { Region } from 'src/app/model/Region';
import { TarjetaProducto } from 'src/app/model/TarjetaProducto';
import { Router } from '@angular/router';
import { Observable, BehaviorSubject } from 'rxjs';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { startWith, map, debounceTime, switchMap } from 'rxjs/operators';
import { MatRadioChange } from '@angular/material/radio';
import { Filtro } from 'src/app/common/model/Filtro';
import { Descriptivo } from 'src/app/common/model/Descriptivo';
import { PuntoConexion } from 'src/app/model/PuntoConexion';

@Component({
	selector: 'app-traslados',
	templateUrl: './traslados.component.html',
	styleUrls: ['./traslados.component.less']
})
export class TrasladosComponent extends SessionComponent implements OnInit {

	trasladoForm: FormGroup;

	filtro: FiltroTraslado = new FiltroTraslado(null, 0, 100);
	searchFilter: Filtro = new Filtro ('dummy', {});
	startDate = new Date();
	puntoConexionDesde: PuntoConexion;
	puntoConexionHasta: PuntoConexion;

	regiones:any[] = [];
	zonas:any[] = [];
	tipos:any[] = [];
	items:TarjetaTraslado[] = [];
	promos:TarjetaTraslado[] = [];
	filteredDesde: BehaviorSubject<any[]> = new BehaviorSubject([]);
	filteredHasta: BehaviorSubject<any[]> = new BehaviorSubject([]);
	slider: string[] = [
		'assets/implementacion/images/1.png',
		'assets/implementacion/images/2.png',
		'assets/implementacion/images/3.png',
		'assets/implementacion/images/4.png'
	]

	region:any;

	cants:number[] = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20];

	constructor(private app: AppComponent, 
			private puntoConexionService:PuntoConexionService,
			private listaPreciosService:ListaPreciosService,
			private pppService: PresentadorPreciosService,
			private regionService: RegionService,
			private router: Router,
			private fb: FormBuilder) {
		super();
		app.activeMenu = 'traslados';
		this.buildForm();
		this.fechaHasta.disable();
	}
	
	async ngOnInit() {
		window.scroll({
			top: 0,
			left: 0,
			behavior: 'smooth'
		});
		this.filtro.esIdaYVuelta = false;
		this.app.menuVisible = true;
		this.regiones = ((await this.regionService.getAll()) || []).filter(x=>x.zonas?.length);
		if (this.regiones.length === 1) this.region = this.regiones[0];
		this.zonas = await this.puntoConexionService.getAll();
		this.tipos = await this.listaPreciosService.getProductos(Constantes.TIPO_PRODUCTO.TRASLADO, this.translateService.idioma ? this.translateService.idioma : 'ES');
		this.tipos = this.tipos.map(t=>t.producto);
		this.buildZonas();
		this.zonaDesde.valueChanges
			.pipe(debounceTime(500),
			map((val) => {
				if (val?.id || val?.codigo) {
					return val;
				} else {
					this.searchFilter.searchStr = val;
					return this._filter(val, 'D');
				}
			})).subscribe(r => {
				return r
			});
		this.zonaHasta.valueChanges
			.pipe(debounceTime(500),
			map((val) => {
				if (val?.id || val?.codigo) {
					return val;
				} else {
					this.searchFilter.searchStr = val;
					return this._filter(val, 'H');
				}
			})).subscribe(r => {
				return r
			});
	}

	detalle(item) {
		if (!this.filtro.producto) {
			this.filtro.producto = new Descriptivo(item.producto.codigo, item.producto.descripcion, item.producto.id);
		}
		this.saveZonas(this.trasladoForm.value);
		console.log(this.filtro);
		console.log("El item: ", item);
		this.app.detalleProducto = item;
		this.app.filtroAplicado = this.filtro;
		this.app.filtroAplicado.zonaDesde = this.puntoConexionDesde
		this.app.filtroAplicado.zonaHasta = this.puntoConexionHasta
		this.router.navigate(["detalle-traslado"]);
	}

	displayFn(zona: any): string {
		return zona && zona.descripcion ? zona.descripcion : '';
	  }

	async buscar(data: any) {
		if (this.trasladoForm.valid) {
			this.puntoConexionDesde = data.zonaDesde;
			this.filtro.zonaDesde = data.zonaDesde.zona;
			this.puntoConexionHasta = data.zonaHasta;
			this.filtro.zonaHasta = data.zonaHasta.zona;
			this.filtro.adultos = data.adultos || 0;
			this.filtro.menores = data.menores || 0;
			this.filtro.producto = data.producto || null;
			this.filtro.fecha = data.fecha;
			if (data.fechaHasta !== undefined) {
				this.filtro.fechaHasta = data.fechaHasta;
				this.filtro.esIdaYVuelta = true;
			}
			this.filtro._idioma = this.translateService.idioma;
			const items = await this.pppService.getTraslados(this.filtro);
			 this.items = items;
			 window.scrollBy(0,500)
		} 
	}

	private async _filter(val: string, origin: string) {
		let data = await this.puntoConexionService.getFiltered(this.searchFilter);
		if (origin === 'D') {
			this.filteredDesde.next(data);
			return this.filteredDesde.value;
		} else if (origin === 'H') {	
			this.filteredHasta.next(data);		
			return this.filteredHasta.value;
		}
	  }

	  buildForm(): void {
		  this.trasladoForm = this.fb.group({
			  esIdaYVuelta: false,
			  zonaDesde: ['', Validators.required],
			  zonaHasta: ['', Validators.required],
			  fecha: ['', Validators.required],
			  fechaHasta: [''],
			  producto: [''],
			  menores: [''],
			  adultos: ['']
		})
	  }

	  get esIdaYVuelta() {
		  return this.trasladoForm.get('esIdaYVuelta');
	  }

	  get fecha() {
		  return this.trasladoForm.get('fecha');
	  }

	  get fechaHasta() {
		  return this.trasladoForm.get('fechaHasta');
	  }

	  get zonaDesde() {
		  return this.trasladoForm.get('zonaDesde');
	  }
	  get zonaHasta() {
		  return this.trasladoForm.get('zonaHasta');
	  }


	  radioChange($event: MatRadioChange): void {
			  if ( $event.value === true) {
				  this.fechaHasta.enable();
			  } else if ( $event.value === false) {
				  this.fechaHasta.disable();
			  }
	  }

	  buildZonas(): void {
		  this.filteredDesde.next(this.zonas);
		  this.filteredHasta.next(this.zonas);
	  }

	  saveZonas(data: any): void {
		  let desde = new Descriptivo(data.zonaDesde.codigo, data.zonaDesde.descripcion,  data.zonaDesde.id);
		  let hasta = new Descriptivo(data.zonaHasta.codigo, data.zonaHasta.descripcion,  data.zonaHasta.id);
		  localStorage.setItem("desde", JSON.stringify(desde));
		  localStorage.setItem("hasta", JSON.stringify(hasta));
	  }
	

}
